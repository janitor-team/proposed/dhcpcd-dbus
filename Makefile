PROG=		dhcpcd-dbus
SRCS=		dhcpcd.c dbus-dict.c dhcpcd-dbus.c wpa.c wpa-dbus.c
SRCS+=		eloop.c main.c

OBJS=		${SRCS:.c=.o}

CFLAGS?=	-O2

TOP?=		.
include ${TOP}/iconfig.mk

CONFFILES=	dhcpcd-dbus.conf
FILES=		name.marples.roy.dhcpcd.service
CONFDIR=	${SYSCONFDIR}/dbus-1/system.d
FILESDIR=	${PREFIX}/share/dbus-1/system-services
CLEANFILES+=	name.marples.roy.dhcpcd.service

DEPEND!=	test -e .depend && echo "depend" || echo ""
VERSION!=	sed -n 's/\#define VERSION[[:space:]]*"\(.*\)".*/\1/p' \
		    ${TOP}/defs.h

FOSSILID?=	current
DISTPREFIX?=	${PROG}-${VERSION}
DISTFILEGZ?=	${DISTPREFIX}.tar.gz
DISTFILE?=	${DISTPREFIX}.tar.bz2

.SUFFIXES: .in

all: ${PROG} ${FILES}

.in:
	${SED} -e 's:@LIBEXECDIR@:${LIBEXECDIR}:g' $@.in > $@

.c.o:
	${CC} ${CFLAGS} ${CPPFLAGS} -c $< -o $@

.depend: ${SRCS} ${COMPAT_SRCS}
	${CC} ${CPPFLAGS} -MM ${SRCS} ${COMPAT_SRCS} > .depend

depend: .depend

${PROG}: ${DEPEND} ${OBJS}
	${CC} ${LDFLAGS} -o $@ ${OBJS} ${LDADD}

test:
	dbus-send --system --print-reply \
	    --dest=name.marples.roy.dhcpcd \
	    /name/marples/roy/dhcpcd \
	    name.marples.roy.dhcpcd.GetInterfaces

proginstall:
	${INSTALL} -d ${DESTDIR}${LIBEXECDIR}
	${INSTALL} -m ${BINMODE} ${PROG} ${DESTDIR}${LIBEXECDIR}

install: proginstall
	${INSTALL} -d ${DESTDIR}${CONFDIR}
	${INSTALL} -m ${FILESMODE} ${CONFFILES} ${DESTDIR}${CONFDIR}
	${INSTALL} -d ${DESTDIR}${FILESDIR}
	${INSTALL} -m ${FILESMODE} ${FILES} ${DESTDIR}${FILESDIR}

clean:
	rm -f ${OBJS} ${PROG} ${PROG}.core ${CLEANFILES}

distclean: clean
	rm -f .depend config.h config.mk *.bz2

dist:
	fossil tarball --name ${DISTPREFIX} ${FOSSILID} ${DISTFILEGZ}
	gunzip -c ${DISTFILEGZ} |  bzip2 >${DISTFILE}
	rm ${DISTFILEGZ}

include Makefile.inc
